In this work-container:

1. VSCoduim (https://vscodium.com/) 
2. AndroidStudio (https://developer.android.com/studio/)
3. Flutter (https://flutter.dev/docs/get-started/install/linux)

How to setup flutter-workspace?

1. install podman (https://podman.io/getting-started/installation)
2. install toolbox (https://github.com/containers/toolbox)
3. Build and Create workspace: 
    1) git clone https://gitlab.com/IvanKem/workspace.git
    2) cd workspace/
    3) podman build -t (<your image tag>) -f toolbox.Dockerfile . 
    4)  toolbox create -c (<your workspace name>) --image (<your image tag>)
    5) toolbox enter (<your workspace name>)

    *example https://blogs.gnome.org/alatiera/2021/03/03/toolbox-your-debian/*

 
