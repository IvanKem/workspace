                                                                                   
export STARSHIP_CONFIG=/etc/starship/config.toml
eval "$(starship init bash)"

alias ls=exa
alias diff='diff --color=auto'
alias grep='grep --color=auto'
alias ip='ip -color=auto'
alias android-studio='/opt/android-studio/bin/studio.sh'

export PATH="$PATH:/opt/flutter/bin"
