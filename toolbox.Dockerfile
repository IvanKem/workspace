FROM registry.fedoraproject.org/fedora-toolbox:34

LABEL name="Liferooter's workspace" \
      version="latest"


RUN sudo dnf upgrade -y
RUN sudo rpm --import https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg

RUN printf "[gitlab.com_paulcarroty_vscodium_repo]\nname=gitlab.com_paulcarroty_vscodium_repo\nbaseurl=https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/rpms/\nenabled=1\ngpgcheck=1\nrepo_gpgcheck=1\ngpgkey=https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg" |sudo tee -a /etc/yum.repos.d/vscodium.repo


RUN sudo dnf install -y \
	curl \
	wget \
	bat \
	exa \
	fd-find \
	codium \
	git \
	meson \
	micro \
	percol \
	clang \
	cmake \
	ninja-build \
	pkg-config \
	gtk3-devel \
	unzip \
	which \
	zip \
	mesa-libGLU
	

RUN curl -fsSL https://starship.rs/install.sh > /opt/install-starship.sh
RUN bash /opt/install-starship.sh --yes > /dev/null
RUN rm /opt/install-starship.sh


COPY bashrc /etc/my_bashrc
RUN echo "source /etc/my_bashrc" >> /etc/bashrc

COPY starship.toml /etc/starship/config.toml

RUN wget https://storage.googleapis.com/flutter_infra_release/releases/stable/linux/flutter_linux_2.2.1-stable.tar.xz
RUN tar -xf flutter_linux_2.2.1-stable.tar.xz -C /opt

RUN sudo chmod -R 777 /opt
RUN flutter precache

RUN wget https://redirector.gvt1.com/edgedl/android/studio/ide-zips/4.2.1.0/android-studio-ide-202.7351085-linux.tar.gz
RUN tar -xf android-studio-ide-202.7351085-linux.tar.gz -C /opt


CMD /bin/sh
